import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""
#test

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[j], Tab[i]
    return Tab

print(melange_carte(Tabl))

def carte_cache(Tab):   #TODO
    """
        TODO
        Doit créer une liste de même longueur que la liste renvoyés dans la question précédentes
    """
    L = []
    for i in range (len(Tabl)):
        L.append(0)
    return L

def choisir_cartes(Tab):
    """
    

    Parameters
    ----------
    Tab : TYPE char list
        liste contenant les valeurs des cartes 

    Returns 
    -------
    int list
       liste contenant les indices des deux cartes choisies. Les valeurs des cartes ne peuvent pas être identiques

    """
    c1 = int(input("Choisissez une carte entre 1 et " + str(len(Tab)) + " : "))
    print(Tab[c1-1])
    c2 = int(input("Choisissez une deuxieme carte entre 1 et " + str(len(Tab)) + " : "))
    while c1.type!=int or c2.type!=int:
        print("Erreur, les entrées doivent être des nombres entiers")
        c1 = int(input("Choisissez une carte (nombre entier): "))
        print(Tab[c1-1])
        c2 = int(input("Choisissez une deuxieme carte (toujours un nombre entier) : "))
    while c1<1 or c1>len(Tab) or c2<1 or c2>len(Tab):
        print("Erreur, les entrées doivent être comprises entre 1 et 12 inclus")
        c1 = int(input("Choisissez une carte (nombre entier): "))
        print(Tab[c1-1])
        c2 = int(input("Choisissez une deuxieme carte (toujours un nombre entier) : "))
    while c1 == c2:
        print("Erreur, la deuxieme carte ne peut être la même que la premiere ! ")
        c2 = int(input("Veuillez choisir une deuxieme carte différente de la premiere : "))
    print(Tab[c2])
    c2 = int(input("Veuillez choisir une deuxieme carte différente de la premiere : "))
    print(Tab[c2])
    return [c1,c2]

   


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
        TODO
        doit retourner les cartes dans la liste cachée
    """
    C1,C2 = Tab[c1],Tab[c2]
    Tab_cache[c1],Tab_cache[c2] = C1,C2
    return Tab_cache


def jouer(Tab):
    """ fonction reprenant les fonctions précédentes melange_carte, carte_cache, choisir_cartes et retourne_carte.
        Elle nous permet ainsi de jouer au jeu. 
        param Tab: list de string
        type Tab: list
    """
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo tu as gagné!!!")

    
#jouer(Tabl)
